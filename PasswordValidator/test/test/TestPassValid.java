package test;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import validator.PassValid;

/* this jUnit test class will test the PassValid class. 
 * Gordon Joyce - 991556227 */
public class TestPassValid {

	@Ignore @Test
	public void test() {
		fail("Not yet implemented");
	}
	
	
	@Test
	public void testValidateRegular()
	{		
		boolean result = PassValid.validate( "passwordsz" );
		assertTrue( "This password is valid.", result );
	}
	
	
	
	@Test (expected=NullPointerException.class)
	public void testValidateException()
	{
		//boolean result = PassValid.validate( null );
		fail( "Password will not be validated." );
	}
	
	
	@Test
	public void testValidateBoundaryIn()
	{
		boolean result = PassValid.validate("password");
		assertTrue( "Password will validate on the boundary.", result );
	}
	
	
	// this was marked wrong, need to check
	@Test
	public void testValidateBoundaryOut()
	{
		boolean result = PassValid.validate( "passwor");// is number validation implemented?
		assertFalse( "Password will not be validated, not in compliance with boundary.", result );
	}
	
	//////////////////////////////////////////////////////////
	
	
	@Test
	public void testValidateNumberRegular()
	{
		boolean result = PassValid.validateNumbers("passwordz123");
		assertTrue( "This password will be validated.", result );
	}
	
	@Test (expected=NullPointerException.class)
	public void testValidateNumbersException()
	{
		//boolean result = PassValid.validateNumbers( null );
		fail( "This method will fail.");
	}
	
	@Test
	public void testValidateNumbersBoundaryIn()
	{
		boolean result = PassValid.validateNumbers( "12" );
		assertTrue( "The password will validate.", result );
	}
	
	@Test
	public void testValidateNumbersBoundaryOut()
	{
		boolean result = PassValid.validateNumbers( "2" );
		assertFalse( "This password will not be validated.", result );
	}

}
