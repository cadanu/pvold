package validator;

/* this class will be created using TDD. It will be used to validate passwords.
 * passwords must have at least 8 characters and contain 2 digits. 
 * Gordon Joyce - 991556227 */
public class PassValid {
	
//	public static boolean validate(String password) throws NumberFormatException, StringIndexOutOfBoundsException
//	{
//		int counter = 0;
//		boolean result = false;
//		char[] arr = password.toCharArray();
//		
//		if(arr.length > 7)
//		{
//			
//			for(int i = 0; i < arr.length; ++i)
//			{
//				String tryout = "" + arr[i];
//				try
//				{
//					Integer.parseInt(tryout);
//					counter += 1;
//				}
//				catch(NumberFormatException nfe)
//				{
//					// handled
//				}
//			}
//			if( counter > 1 ) result = true;
//			
//		}
//		return result;
//	}
	
//	public static boolean validate(String password) 
//			throws NumberFormatException, StringIndexOutOfBoundsException, NullPointerException
//	{
//		int counter = 0;
//		boolean result = false;
//		char[] arr = password.toCharArray();
//		
//		if( password.equals(null) ) //NullPointerException will be thrown --should be handled;
//		if( password.length() > 7 )
//		{
//			
//			for(int i = 0; i < arr.length; ++i)
//				{
//					String tryout = "" + arr[i];
//					try
//					{
//						Integer.parseInt(tryout);
//						counter += 1;
//					}
//					catch(NumberFormatException nfe)
//					{
//						System.out.print("Exception: " + nfe.getMessage());// handled
//					}
//				}
//				if( counter > 1 ) result = true;
//		}		
//		return result;
//	}
	
	public static boolean validate(String password) 
			throws NumberFormatException, StringIndexOutOfBoundsException, NullPointerException
	{
		//int count = 0;
		boolean result = false;		
		if( password.trim().length() > 7 ) result = true;
		
		return result;
	}
	
	public static boolean validateNumbers(String password) 
			throws NumberFormatException, StringIndexOutOfBoundsException, NullPointerException
	{
		int counter = 0;
		boolean result = false;
		for(int i = 0; i < password.trim().length(); ++i)
			{
				String temp = "" + password.trim().charAt(i);
				try
				{
					Integer.parseInt(temp);
					counter += 1;
				}
				catch(NumberFormatException nfe)
				{
					System.out.print("Exception: " + nfe.getMessage());// handled
				}
			}
			if( counter > 1 ) result = true;
		return result;
	}

}
